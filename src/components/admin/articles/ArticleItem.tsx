import {Article} from "../../../types.ts";
import React from "react";
import {Link} from "react-router-dom";


interface ArticleItemProps {
    article: Article;
    onDelete: (id: string) => void;
}

export const ArticleItem: React.FC<ArticleItemProps> = ({ article, onDelete }) => {
    const totalQuantity = Object.values(article.quantites).reduce((acc, qty) => acc + qty, 0);

    return (
        <tr>
            <td>{article.reference}</td>
            <td>{article.nom}</td>
            <td>{article.marque}</td>
            <td>{article.categorie}</td>
            <td>{article.prix['$numberDecimal'] ? article.prix['$numberDecimal'] : article.prix}$</td>
            <td>{totalQuantity}</td>
            <td>{article.rayon_id}</td>
            <td>{article.fournisseur.nom}</td>
            <td>
                <Link className="btn btn-sm" to={`/admin/articles/${article._id}/edit`}>Edit</Link>
                <button className="btn btn-sm btn-error" onClick={() => onDelete(article._id)}>Delete</button>
            </td>
        </tr>
    );
};