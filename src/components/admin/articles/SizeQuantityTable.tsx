import React, { useEffect, useState } from 'react';
import { Icon } from '@iconify/react';

interface SizeQuantity {
    size: string;
    quantity: number;
}

interface SizeQuantityTableProps {
    value?: Record<string, number>;
    onChange: (quantities: Record<string, number>) => void;
}

export const SizeQuantityTable: React.FC<SizeQuantityTableProps> = ({ value = {}, onChange }) => {
    const [sizes, setSizes] = useState<SizeQuantity[]>([]);

    useEffect(() => {
        const initialSizes = Object.entries(value).map(([size, quantity]) => ({ size, quantity }));
        setSizes(initialSizes);
    }, [value]);

    const [sizeInput, setSizeInput] = useState('');
    const [quantityInput, setQuantityInput] = useState(0);

    const handleAddSize = () => {
        const newSizeQuantity = { size: sizeInput.toUpperCase(), quantity: quantityInput };

        const existingSizeIndex = sizes.findIndex(sq => sq.size === sizeInput.toUpperCase());

        let updatedSizes;
        if (existingSizeIndex !== -1) {
            updatedSizes = [...sizes];
            updatedSizes[existingSizeIndex] = newSizeQuantity;
        } else {
            updatedSizes = [...sizes, newSizeQuantity];
        }

        setSizes(updatedSizes);
        setSizeInput('');
        setQuantityInput(0);

        const quantities = updatedSizes.reduce((acc, sq) => {
            acc[sq.size] = sq.quantity;
            return acc;
        }, {} as Record<string, number>);
        onChange(quantities);
    };

    const handleRemoveSize = (size: string) => {
        const updatedSizes = sizes.filter(sq => sq.size !== size);
        setSizes(updatedSizes);

        const quantities = updatedSizes.reduce((acc, sq) => {
            acc[sq.size] = sq.quantity;
            return acc;
        }, {} as Record<string, number>);
        onChange(quantities);
    };

    // Sort sizes to ensure the smallest quantity is always the last row
    const sortedSizes = [...sizes].sort((a, b) => a.quantity - b.quantity);

    return (
        <div className="max-w-sm">
            <div className="join">
                <input
                    type="text"
                    placeholder="Size"
                    value={sizeInput}
                    className="input join-item"
                    onChange={(e) => setSizeInput(e.target.value)}
                />
                <input
                    type="number"
                    placeholder="Quantity"
                    className="input join-item"
                    value={quantityInput}
                    onChange={(e) => setQuantityInput(Number(e.target.value))}
                />
                <button type="button" className="btn btn-square btn-primary" onClick={handleAddSize}>
                    <Icon icon="mdi:plus" />
                </button>
            </div>
            <table className="table">
                <thead>
                <tr>
                    <th>Size</th>
                    <th>Quantity</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {sortedSizes.map((sq, index) => (
                    <tr key={index}>
                        <td>{sq.size}</td>
                        <td>{sq.quantity}</td>
                        <td>
                            <button
                                type="button"
                                className="btn btn-square btn-xs btn-error"
                                onClick={() => handleRemoveSize(sq.size)}
                            >
                                <Icon icon="mdi:minus" />
                            </button>
                        </td>
                    </tr>
                ))}
                </tbody>
            </table>
        </div>
    );
};
