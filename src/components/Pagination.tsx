import React from 'react';

interface PaginationProps {
    currentPage: number;
    totalPages: number;
    onPageChange: (page: number) => void;
}

export const Pagination: React.FC<PaginationProps> = ({ currentPage, totalPages, onPageChange }) => {
    const renderPagination = () => {
        if (totalPages <= 6) {
            return (
                Array.from({ length: totalPages }, (_, i) => (
                    <button
                        key={i + 1}
                        className={`join-item btn ${i + 1 === currentPage ? 'btn-primary' : ''}`}
                        onClick={() => onPageChange(i + 1)}
                    >
                        {i + 1}
                    </button>
                ))
            );
        }

        const pages = [];
        pages.push(
            <button
                key={1}
                className={`join-item btn ${1 === currentPage ? 'btn-primary' : ''}`}
                onClick={() => onPageChange(1)}
            >
                1
            </button>
        );

        if (currentPage > 3) {
            pages.push(<span key="start-ellipsis" className="join-item btn">...</span>);
        }

        for (let i = Math.max(2, currentPage - 1); i <= Math.min(totalPages - 1, currentPage + 1); i++) {
            pages.push(
                <button
                    key={i}
                    className={`join-item btn ${i === currentPage ? 'btn-primary' : ''}`}
                    onClick={() => onPageChange(i)}
                >
                    {i}
                </button>
            );
        }

        if (currentPage < totalPages - 2) {
            pages.push(<span key="end-ellipsis" className="join-item btn">...</span>);
        }

        pages.push(
            <button
                key={totalPages}
                className={`join-item btn ${totalPages === currentPage ? 'btn-primary' : ''}`}
                onClick={() => onPageChange(totalPages)}
            >
                {totalPages}
            </button>
        );

        return pages;
    };

    return (
        <div className="join">
            {renderPagination()}
        </div>
    );
};
