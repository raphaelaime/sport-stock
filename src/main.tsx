import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import {createBrowserRouter, RouterProvider} from "react-router-dom";
import {Home} from "@routes/Home.tsx";
import {Articles} from "@routes/admin/Articles.tsx";
import {Rayons} from "@routes/admin/Rayons.tsx";
import {Root} from "@routes/Root.tsx";
import {NewArticle} from "@routes/admin/NewArticle.tsx";
import {EditArticle} from "@routes/admin/EditArticle.tsx";


const router = createBrowserRouter([
    {
        path: '/',
        element: <Root />,
        children: [
            {
                path: '/',
                element: <Home />
            },
            {
                path: '/admin/articles',
                element: <Articles />,
            },
            {
                path: '/admin/articles/new',
                element: <NewArticle />
            },
            {
                path: '/admin/articles/:id/edit',
                element: <EditArticle />
            },
            {
                path: '/admin/rayons',
                element: <Rayons />
            }
        ]
    },
    {path: '/admin/articles', element: <Articles />},
    {path: '/admin/rayons', element: <Rayons />}
])

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
