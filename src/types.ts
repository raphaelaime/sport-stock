export interface Fournisseur {
    nom: string;
    ville: string;
}

export interface Article {
    _id: string;
    marque: string;
    prix: number;
    nom: string;
    reference: string;
    categorie: 'enfant' | 'junior' | 'senior';
    quantites: { [size: string]: number };
    fournisseur: Fournisseur;
    rayon_id: string;
}

export interface Rayon {
    _id: string;
    id: string;
    description: string;
    employeResponsable: string;
}
