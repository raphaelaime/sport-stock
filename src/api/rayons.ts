import axios from 'axios';

export const fetchRayons = async (page: number) => {
    const response = await axios.get(`http://localhost:3000/api/rayons?page=${page}&limit=10`);
    return response.data;
};

export const deleteRayon = async (id: string) => {
    const response = await axios.delete(`http://localhost:3000/api/rayons/${id}`);
    return response.data;
};

export const checkRayonIdExists = async (rayonId: string) => {
    const response = await axios.get(`http://localhost:3000/api/rayons/${rayonId}`);
    return response.data;
};
