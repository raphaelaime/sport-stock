import axios from 'axios';
import {Article} from "../types.ts";

interface FetchArticlesResponse {
    total: number;
    pages: number;
    currentPage: number;
    articles: Article[];
}

export const fetchArticles = async (page: number, search = '', sortField = '', sortOrder = 'asc'): Promise<FetchArticlesResponse> => {
    const response = await axios.get('http://localhost:3000/api/articles', {
        params: {
            page,
            search,
            sortField,
            sortOrder,
        },
    });
    return response.data;
};

export const deleteArticle = async (id: string) => {
    const response = await axios.delete(`http://localhost:3000/api/articles/${id}`);
    return response.data;
};

export const addArticle = async (article: Omit<Article, '_id'>) => {
    const response = await axios.post(`http://localhost:3000/api/articles`, article);
    return response.data;
};

export const updateArticle = async (id: string, article: Omit<Article, '_id'>) => {
    const response = await axios.put(`http://localhost:3000/api/articles/${id}`, article);
    return response.data;
};

export const fetchArticleById = async (id: string) => {
    const response = await axios.get(`http://localhost:3000/api/articles/${id}`);
    return response.data;
};