import { useRef} from 'react';
import { Link, Outlet } from 'react-router-dom';
import {QueryClient, QueryClientProvider} from "@tanstack/react-query";

const queryClient = new QueryClient();

export const Root = () => {
    const detailsRef = useRef<HTMLDetailsElement>(null);

    const handleLinkClick = () => {
        if (detailsRef.current) {
            detailsRef.current.open = false;
        }
    };

    return (
        <>
            <QueryClientProvider client={queryClient}>
                <nav className="navbar bg-base-100">
                    <div className="flex-1">
                        <a className="btn btn-ghost text-xl">SportStock</a>
                    </div>
                    <div className="flex-none">
                        <ul className="menu menu-horizontal px-1">
                            <li>
                                <Link to="/">Home</Link>
                            </li>
                            <li>
                                <details ref={detailsRef}>
                                    <summary>
                                        Admins
                                    </summary>
                                    <ul className="p-2 bg-base-100 rounded-t-none">
                                        <li>
                                            <Link to="/admin/articles" onClick={handleLinkClick}>Articles</Link>
                                        </li>
                                        <li>
                                            <Link to="/admin/rayons" onClick={handleLinkClick}>Rayons</Link>
                                        </li>
                                    </ul>
                                </details>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div className="container mx-auto p-4">
                    <Outlet />
                </div>
            </QueryClientProvider>
        </>
    );
};
