import React, { useEffect, useState } from 'react';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { SizeQuantityTable } from '@components/admin/articles/SizeQuantityTable';
import { Article } from "../../types.ts";
import { fetchArticleById, updateArticle } from "@api/articles.ts";
import { checkRayonIdExists } from "@api/rayons.ts";
import { useParams, useNavigate } from 'react-router-dom';

export const EditArticle: React.FC = () => {
    const { id } = useParams<{ id: string }>(); // Access the id parameter
    const navigate = useNavigate();
    const [article, setArticle] = useState<Omit<Article, '_id'>>({
        marque: '',
        prix: 0,
        nom: '',
        reference: '',
        categorie: 'enfant',
        quantites: {},
        fournisseur: { nom: '', ville: '' },
        rayon_id: ''
    });
    const [rayonError, setRayonError] = useState<string | null>(null);

    const queryClient = useQueryClient();

    const { data: fetchedArticle, isLoading: isFetching, isError: isFetchingError } = useQuery(
        {
            queryKey: ['article', id],
            queryFn: () => fetchArticleById(id!),
            notifyOnChangeProps: ['data'],
        });

    useEffect(() => {
        if (fetchedArticle) {
            setArticle({...fetchedArticle, prix: fetchedArticle.prix['$numberDecimal']});
        }
    }, fetchedArticle);

    const mutation = useMutation({
        mutationFn: (updatedArticle: Omit<Article, '_id'>) => updateArticle(id!, updatedArticle),
        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: ['articles'] });
            navigate('/admin/articles');
        },
    });

    const rayonQuery = useQuery({
        queryKey: ['checkRayonId', article.rayon_id],
        queryFn: () => checkRayonIdExists(article.rayon_id),
        enabled: !!article.rayon_id,
        notifyOnChangeProps: ['isError'],
    });

    const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
        const { name, value } = e.target;
        setArticle(prevState => ({
            ...prevState,
            [name]: name === 'prix' ? parseFloat(value) : value,
        }));
    };

    const handleFournisseurChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;
        setArticle(prevState => ({
            ...prevState,
            fournisseur: {
                ...prevState.fournisseur,
                [name]: value,
            },
        }));
    };

    const handleQuantitiesChange = (quantities: Record<string, number>) => {
        setArticle(prevState => ({
            ...prevState,
            quantites: quantities,
        }));
    };

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (rayonQuery.isError) {
            setRayonError('Rayon invalide !');
            return;
        }
        mutation.mutate(article);
    };

    if (isFetching) {
        return <p>Loading...</p>;
    }

    if (isFetchingError) {
        return <p>Error fetching article details</p>;
    }

    return (
        <div>
            <h2 className="text-3xl font-bold mb-8">Modifier l'article</h2>
            <form onSubmit={handleSubmit}>
                <div className="grid grid-cols-3 gap-5">
                    <div>
                        <label className="input input-bordered flex items-center gap-2">
                            Marque
                            <input type="text" name="marque" className="grow" value={article.marque}
                                   onChange={handleChange} required />
                        </label>
                    </div>
                    <div>
                        <label className="input input-bordered flex items-center gap-2">
                            Prix
                            <input type="number" step="0.01" name="prix" value={article.prix} onChange={handleChange}
                                   required
                                   className="grow" />
                        </label>
                    </div>
                    <div>
                        <label className="input input-bordered flex items-center gap-2">
                            Nom
                            <input type="text" name="nom" value={article.nom} onChange={handleChange} required
                                   className="grow" />
                        </label>
                    </div>
                    <div>
                        <label>Categorie:</label>
                        <select name="categorie" value={article.categorie} onChange={handleChange} required>
                            <option value="enfant">Enfant</option>
                            <option value="junior">Junior</option>
                            <option value="senior">Senior</option>
                        </select>
                    </div>
                    <div className="flex flex-col gap-y-3">
                        <span className="font-bold text-xl">Fournisseur</span>
                        <label className="input input-bordered flex items-center gap-2">
                            Nom
                            <input type="text" name="nom" value={article.fournisseur.nom}
                                   onChange={handleFournisseurChange}
                                   placeholder="Nom" required
                                   className="grow" />
                        </label>
                        <label className="input input-bordered flex items-center gap-2">
                            Ville
                            <input type="text" name="ville" value={article.fournisseur.ville}
                                   onChange={handleFournisseurChange}
                                   placeholder="Ville" required
                                   className="grow" />
                        </label>
                    </div>
                    <div>
                        <label className="input input-bordered flex items-center gap-2">
                            ID de rayon
                            <input type="text" name="rayon_id" value={article.rayon_id} onChange={handleChange} required
                                   className="grow" />
                        </label>
                        {rayonQuery.isError && <p className="text-red-500">Rayon invalide !</p>}
                    </div>
                    <div>
                        <span className="font-bold text-xl">Quantitées</span>
                        <SizeQuantityTable onChange={handleQuantitiesChange} value={article.quantites} />
                    </div>
                    <div></div>
                    <button type="submit" className="btn btn-primary mt-5 max-w-xs mx-auto">Validé</button>
                </div>
            </form>
            {mutation.isLoading && <p>Updating article...</p>}
            {mutation.isError && <p>Error updating article: {mutation.error.message}</p>}
            {mutation.isSuccess && <p>Article updated successfully!</p>}
        </div>
    );
};
