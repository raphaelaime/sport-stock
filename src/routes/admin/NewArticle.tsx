import React, {useEffect, useState} from 'react';
import { useMutation, useQueryClient, useQuery } from '@tanstack/react-query';
import { SizeQuantityTable } from '@components/admin/articles/SizeQuantityTable';
import { Article } from "../../types.ts";
import { addArticle } from "@api/articles.ts";
import { checkRayonIdExists } from "@api/rayons.ts";

export const NewArticle: React.FC = () => {
    const [article, setArticle] = useState<Omit<Article, '_id'>>({
        marque: '',
        prix: 0,
        nom: '',
        reference: '',
        categorie: 'enfant',
        quantites: {},
        fournisseur: { nom: '', ville: '' },
        rayon_id: ''
    });

    const queryClient = useQueryClient();

    const mutation = useMutation({
        mutationFn: addArticle,
        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: ['articles'] });
            // Optionally reset form or redirect after success
        },
    });

    const rayonQuery = useQuery({
        queryKey: ['checkRayonId', article.rayon_id],
        queryFn: () => checkRayonIdExists(article.rayon_id),
        enabled: !!article.rayon_id,
        notifyOnChangeProps: ['isError'],
    });

    const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
        const { name, value } = e.target;
        setArticle(prevState => ({
            ...prevState,
            [name]: name === 'prix' ? parseFloat(value) : value,
        }));
    };

    const handleFournisseurChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;
        setArticle(prevState => ({
            ...prevState,
            fournisseur: {
                ...prevState.fournisseur,
                [name]: value,
            },
        }));
    };

    const handleQuantitiesChange = (quantities: Record<string, number>) => {
        setArticle(prevState => ({
            ...prevState,
            quantites: quantities,
        }));
    };

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (!article.rayon_id || rayonQuery.isError) {
            return;
        }
        mutation.mutate({ ...article, prix: parseFloat(article.prix.toString()) });
    };

    return (
        <div>
            <h2 className="text-3xl font-bold mb-8">Ajouter un nouvelle article</h2>
            <form onSubmit={handleSubmit}>
                <div className="grid grid-cols-3 gap-5">
                    <div>
                        <label className="input input-bordered flex items-center gap-2">
                            Marque
                            <input type="text" name="marque" className="grow" value={article.marque}
                                   onChange={handleChange} required />
                        </label>
                    </div>
                    <div>
                        <label className="input input-bordered flex items-center gap-2">
                            Prix
                            <input type="number" step="0.01" name="prix" value={article.prix} onChange={handleChange}
                                   required
                                   className="grow" />
                        </label>
                    </div>
                    <div>
                        <label className="input input-bordered flex items-center gap-2">
                            Nom
                            <input type="text" name="nom" value={article.nom} onChange={handleChange} required
                                   className="grow" />
                        </label>
                    </div>
                    <div>
                        <label>Categorie:</label>
                        <select name="categorie" value={article.categorie} onChange={handleChange} className={"select select-bordered"} required>
                            <option value="enfant">Enfant</option>
                            <option value="junior">Junior</option>
                            <option value="senior">Senior</option>
                        </select>
                    </div>
                    <div className="flex flex-col gap-y-3">
                        <span className="font-bold text-xl">Fournisseur</span>
                        <label className="input input-bordered flex items-center gap-2">
                            Nom
                            <input type="text" name="nom" value={article.fournisseur.nom}
                                   onChange={handleFournisseurChange}
                                   placeholder="Nom" required
                                   className="grow" />
                        </label>
                        <label className="input input-bordered flex items-center gap-2">
                            Ville
                            <input type="text" name="ville" value={article.fournisseur.ville}
                                   onChange={handleFournisseurChange}
                                   placeholder="Ville" required
                                   className="grow" />
                        </label>
                    </div>
                    <div>
                        <label className="input input-bordered flex items-center gap-2">
                            ID de rayon
                            <input type="text" name="rayon_id" value={article.rayon_id} onChange={handleChange} required
                                   className="grow" />
                        </label>
                        {rayonQuery.isError && <p className="text-red-500">Rayon invalide !</p>}
                    </div>
                    <div>
                        <span className="font-bold text-xl">Quantitées</span>
                        <SizeQuantityTable onChange={handleQuantitiesChange} value={article.quantites} />
                    </div>
                    <div></div>
                    <div>
                        <button type="submit" className="btn btn-primary mt-5 max-w-xs mx-auto">Validé</button>
                        {mutation.isLoading && <p>Ajout de l'article...</p>}
                        {mutation.isError && <p>Erreur à l'ajout: {mutation.error.message}</p>}
                        {mutation.isSuccess && <p>Article ajouter avec succès!</p>}
                    </div>
                </div>
            </form>
        </div>
    );
};
