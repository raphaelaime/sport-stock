import React, { useState } from 'react';
import { Article } from '../../types';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { ArticleItem } from '@components/admin/articles/ArticleItem';
import { Pagination } from '@components/Pagination';
import { deleteArticle, fetchArticles } from '@api/articles';
import { Link } from 'react-router-dom';
import { Icon } from '@iconify/react';

export const Articles = () => {
    const [currentPage, setCurrentPage] = useState(1);
    const [search, setSearch] = useState('');
    const [sortField, setSortField] = useState('');
    const [sortOrder, setSortOrder] = useState<'asc' | 'desc'>('asc');
    const queryClient = useQueryClient();

    const { data, isLoading, isError, error } = useQuery({
        queryKey: ['articles', currentPage, search, sortField, sortOrder],
        queryFn: () => fetchArticles(currentPage, search, sortField, sortOrder),
    });

    const deleteMutation = useMutation({
        mutationFn: deleteArticle,
        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: ['articles', currentPage, search, sortField, sortOrder] });
        },
    });

    const handlePageChange = (page: number) => {
        if (page !== currentPage) {
            setCurrentPage(page);
        }
    };

    const handleDelete = async (id: string) => {
        const isLastElementOnPage = data.articles.length === 1;

        await deleteMutation.mutateAsync(id);

        if (isLastElementOnPage && currentPage > 1) {
            setCurrentPage(currentPage - 1);
        } else {
            queryClient.invalidateQueries({ queryKey: ['articles', currentPage, search, sortField, sortOrder] });
        }
    };

    const handleSearchChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setSearch(e.target.value);
    };

    const handleSortChange = (field: string) => {
        const order = sortField === field && sortOrder === 'asc' ? 'desc' : 'asc';
        setSortField(field);
        setSortOrder(order);
    };

    const renderSortIcon = (field: string) => {
        if (sortField === field) {
            return sortOrder === 'asc' ? <Icon icon="mdi:arrow-drop-up" /> : <Icon icon="mdi:arrow-drop-down" />;
        }
        return null;
    };

    return (
        <div>
            <div className="flex justify-between items-center join mb-4">
                <input
                    type="text"
                    placeholder="Search..."
                    className="input input-bordered"
                    value={search}
                    onChange={handleSearchChange}
                />
                <button className="btn btn-primary">
                    <Link to="/admin/articles/new">
                        Ajouter un articles
                    </Link>
                </button>
            </div>

            <div className="overflow-x-auto">
                <table className="table">
                    <thead>
                    <tr>
                        <th onClick={() => handleSortChange('reference')}>Ref {renderSortIcon('reference')}</th>
                        <th onClick={() => handleSortChange('nom')}>Nom {renderSortIcon('nom')}</th>
                        <th onClick={() => handleSortChange('marque')}>Marque {renderSortIcon('marque')}</th>
                        <th onClick={() => handleSortChange('categorie')}>Catégorie {renderSortIcon('categorie')}</th>
                        <th onClick={() => handleSortChange('prix')}>Prix {renderSortIcon('prix')}</th>
                        <th>Qqt Total</th>
                        <th onClick={() => handleSortChange('rayon_id')}>Rayon {renderSortIcon('rayon_id')}</th>
                        <th onClick={() => handleSortChange('fournisseur.nom')}>Fournisseur {renderSortIcon('fournisseur.nom')}</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {isLoading ? (
                        <tr>
                            <td colSpan={9} className="text-center">Loading...</td>
                        </tr>
                    ) : isError ? (
                        <tr>
                            <td colSpan={9} className="text-center">Error: {error.message}</td>
                        </tr>
                    ) : (
                        data.articles.map((article: Article) => (
                            <ArticleItem key={article._id} article={article} onDelete={handleDelete} />
                        ))
                    )}
                    </tbody>
                </table>
            </div>

            <div className="join flex justify-center w-full">
                {!isLoading && !isError && (
                    <Pagination
                        currentPage={currentPage}
                        totalPages={data.pages}
                        onPageChange={handlePageChange}
                    />
                )}
            </div>
        </div>
    );
};
